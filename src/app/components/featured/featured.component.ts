import { Component, OnInit } from '@angular/core';
import { Movie } from '../../models/movie';
import { MoviesService } from '../../services/movies.service';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.css']
})
export class FeaturedComponent implements OnInit {

  movies: Movie[] = [];

  constructor(private _movieService: MoviesService) { }

  ngOnInit() {
    this._movieService.getFeaturedMovies().subscribe(data => {

      this.movies.push(data[0]);
      this.movies.push(data[1]);
    });
  }

}
