import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { Movie } from '../../models/movie';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchString: string;
  movies: Movie[] = [];
  totalResults: number;
  errorMsg: string;
  plot: string = "full";

  constructor(private _moviesService: MoviesService, private _router: Router) { }

  ngOnInit() {
  }

  searchMovies() {
    this.errorMsg = "";
    this._moviesService.searchMovies(this.searchString)
      .subscribe(data => {
        this.movies = data.Search;
        this.totalResults = data.totalResults;
      },
        error => this.errorMsg = error
      );
  }

  movieSelected(imdbID: string) {
    this._router.navigate(["/movies", imdbID, this.plot]);
  }
}
