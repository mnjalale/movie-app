import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { MoviesService } from '../../services/movies.service';
import { Movie } from '../../models/movie';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  movie: Movie = new Movie();
  limit: number = 200;
  truncating = true;
  writers: string[];
  actors: string[];

  constructor(private _route: ActivatedRoute
    , private _movieService: MoviesService
    , private _router: Router) { }

  ngOnInit() {
    this._route.paramMap.subscribe((params: ParamMap) => {
      let id = params.get("id");
      let plot = params.get("plot");

      this._movieService.getMovie(id, plot).subscribe(data => {
        this.movie = data;
        this.writers = data.Writer.split(",");
        this.actors = data.Actors.split(",");
      });
    });
  }

  goBackToSearch() {
    this._router.navigate(["/search"]);
  }

}
