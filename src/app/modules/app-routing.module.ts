import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component';
import { FeaturedComponent } from '../components/featured/featured.component';
import { SearchComponent } from '../components/search/search.component';
import { MovieDetailComponent } from '../components/movie-detail/movie-detail.component';

const routes: Routes = [
  { path: "", redirectTo: '/search', pathMatch: 'full' },
  { path: "featured", component: FeaturedComponent },
  { path: "search", component: SearchComponent },
  { path: "movies/:id/:plot", component: MovieDetailComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
