import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Movie } from '../models/movie';
import { SearchResult } from '../models/searchResult';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private _baseUrl: string = "http://www.omdbapi.com/?apiKey=6c3a2d45&"

  constructor(private _http: HttpClient) { }

  searchMovies(searchString: string, page: number = 1): Observable<SearchResult> {
    let url = this._baseUrl + "s=" + searchString + "&type=movie&page=" + page + "";

    return this._http.get<SearchResult>(url)
      .pipe(catchError(this.errorHandler));
  }

  getMovie(imdbID: string, plot: string = "full"): Observable<Movie> {
    let url = this._baseUrl + "i=" + imdbID + "&plot=" + plot + " ";

    return this._http.get<Movie>(url)
      .pipe(catchError(this.errorHandler));
  }

  getFeaturedMovies(): Observable<[Movie, Movie]> {
    let batmanUrl = this._baseUrl + "t=batman&type=movie&plot=full";
    let supermanUrl = this._baseUrl + "t=superman&type=movie&plot=full";

    let batmanRequest = this._http.get<Movie>(batmanUrl);
    let superemanRequest = this._http.get<Movie>(supermanUrl);

    return forkJoin([batmanRequest, superemanRequest])
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || "Server error");
  }



}
